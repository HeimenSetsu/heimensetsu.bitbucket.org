var localStorage = window.localStorage;

var clickLock = false;

if (!localStorage.daysOpened) {
	localStorage.setItem('daysOpened', JSON.stringify({}));
}

function isDayOpened(date) {
	var daysOpened = JSON.parse(localStorage.getItem('daysOpened'));

	return daysOpened[date];
}

function openDay(date) {
	var daysOpened = JSON.parse(localStorage.getItem('daysOpened'));
	
	daysOpened[date] = true;
	
	localStorage.setItem('daysOpened', JSON.stringify(daysOpened));
}

function createElement(tagName, attributes, content) {
	attributes = attributes || {};
	var element = document.createElement(tagName);
	
	for (var propertyName in attributes) {
		element.setAttribute(propertyName, attributes[propertyName]);
	}
	
	if (content != undefined) {
		element.innerHTML = content;		
	}
	
	return element;
	
}

function createDay(dayData) {
	var day = createElement('div', {
		id: 'calendar-day-' + dayData.date,
		'class': 'calendar-day'
	});
	
	var content = createElement('div', {});

	content.appendChild(createElement('div', {
		'class': 'inset-shadow'
	}));
	content.appendChild(createElement('div', {}, dayData.date));
	day.appendChild(createElement('img', {
		src: (isDayOpened(date) ? dayData.picture : '')
	}));
	
	day.appendChild(content);
	
	day.addEventListener('click', function () {
		showDay(dayData.date);
	});
	
	return day;
}

function createDayModal(dayData) {
	var day = createElement('div', {
		'class': 'modal'
	});
	
	var container = createElement('div', {});
	
	var content = createElement('div', {
		'class': 'modal-content'
	});

	var modalHeader = createElement('div', {
		'class': 'modal-header'
	});
	var titleContainer = createElement('div', {
		'class': 'title-container'
	});
	
	/*modalHeader.appendChild(createElement('div', {
		'class': 'date'
	}, dayData.date));*/
	//modalHeader.appendChild(titleContainer);
	content.appendChild(createElement('div', {
		'class': 'title'
	}, dayData.title));
	content.appendChild(createElement('img', {
		src: dayData.picture
	}));
	content.appendChild(createElement('div', {
		'class': 'comment'
	}, dayData.comment));
	
	container.appendChild(content);
	day.appendChild(createElement('div', {
		'class': 'inset-shadow'
	}));
	day.appendChild(container);
	
	return day;
}

function animateCalendarDay(date, animation) {
	var calendarDay = document.getElementById('calendar-day-' + date);
	
	var calendarDayImage = document.querySelector('#calendar-day-' + date + ' img');
	calendarDayImage.src = getDayData(date).picture;
	
	calendarDay.className += ' opened animation-' + (animation == undefined ? date : animation);	
}

function restoreCalendarDay(date) {
	var calendarDay = document.getElementById('calendar-day-' + date);
	
	calendarDay.className = 'calendar-day' + (isDayOpened(date) ? ' opened' : '');
}

var overlay = document.getElementById('overlay');
var overlayContent = document.getElementById('overlay-content');
function toggleOverlay(isVisible) {
	if (isVisible === undefined) {
		isVisible = !(overlay.style.opacity == '1');
	}
	
	overlay.style.opacity = (isVisible ? 1 : 0);
	overlay.style.pointerEvents = (isVisible ? 'auto' : 'none');
	overlayContent.style.opacity = overlay.style.opacity;	
	
	if (!isVisible) {
		for (var i = 1; i <= 25; ++i) {
			restoreCalendarDay(i);
		}		
	}
}

function showDay(date) {
	var now = new Date();
	
	// now = new Date(2017, 11, 28, 18, 0, 0);
	
	if (now >= new Date(2017, 11, date, 18, 0, 0)) {
		console.log('You can see', date);
	}
	else {
		console.log('You cannot see', date, '(' + now + ' < ' + new Date(2017, 11, date, 18, 0, 0) + ')');
		return;
	}
	
	var dayAlreadyOpened = isDayOpened(date);
	
	openDay(date);
	
	clickLock = true;
	
	animateCalendarDay(date, (date - 1) % 5);
	
	var dayData = getDayData(date);
	overlayContent.innerHTML = '';
	overlayContent.appendChild(createDayModal(dayData));
	
	window.setTimeout(function () {
		toggleOverlay(true);
		clickLock = false;
	}, (dayAlreadyOpened ? 0 : 1200));	
}

var calendarContent = document.getElementById('calendar');

var dayDatas = [];

dayDatas[1] = {
	date: 1,
	title: 'Collection de pots de confitures',
	picture: 'images/1.jpg',
	comment: 'De la fraise à la pêche en passant par la prune, sans oublier les pots de pesto qui se sont bien déguisés !'
};
dayDatas[2] = {
	date: 2,
	title: 'Ornement en forme de dragon',
	picture: 'images/2.jpg',
	comment: 'Il y en a un deuxième sur le pilier de droite, mais en un peu plus détérioré.'
};
dayDatas[3] = {
	date: 3,
	title: 'Parking pour canard',
	picture: 'images/3.jpg',
	comment: 'En dehors du cadre, certains étaient garés en double file...'
};
dayDatas[4] = {
	date: 4,
	title: 'Les plus grands dominos du monde',
	picture: 'images/4.jpg',
	comment: 'Il faut des machines de chantier pour les mettre en place !'
};
dayDatas[5] = {
	date: 5,
	title: 'Dalle fleurie sur un pont piéton',
	picture: 'images/5.png',
	comment: 'Ce pont en a 7 modèles différents, et il y en a encore d\'autres ailleurs.'
};
dayDatas[6] = {
	date: 6,
	title: 'Boîte aux lettres de la poste japonaise',
	picture: 'images/6.jpg',
	comment: 'A gauche pour les lettres et cartes postales, à droite pour le reste.'
};
dayDatas[7] = {
	date: 7,
	title: 'OLIO Takaido',
	picture: 'images/7.jpg',
	comment: 'Ma nouvelle résidence. Le nom de la ville, Takaido, signifie "grand puit".'
};
dayDatas[8] = {
	date: 8,
	title: 'A l\'entrée d\'un temple',
	picture: 'images/8.jpg',
	comment: 'C\'est quand même une grosse lanterne, et sa jumelle est juste à gauche...'
};
dayDatas[9] = {
	date: 9,
	title: 'Monsieur et madame "Trois rivères"',
	picture: 'images/9.jpg',
	comment: 'Parce qu\'au Japon, on peut écrire son nom de famille avec deux coups de griffes.'
};
dayDatas[10] = {
	date: 10,
	title: 'Originale ou à l\'ancienne',
	picture: 'images/10.jpg',
	comment: 'Mais pas de version "Fins gourmets". 6,16€ le gros pot, 3,07€ à l\'ancienne.'
};
dayDatas[11] = {
	date: 11,
	title: 'JÄTTENDAL',
	picture: 'images/11.jpg',
	comment: 'Les lampes design c\'est sympa, mais le rayon enfant c\'est mieux !'
};
dayDatas[12] = {
	date: 12,
	title: 'Tableau de présence',
	picture: 'images/12.jpg',
	comment: 'Le premier arrivé déplace l\'aimant à gauche, le dernier à partir le remet à droite.'
};
dayDatas[13] = {
	date: 13,
	title: 'Au sol de la station Takaido',
	picture: 'images/13.jpg',
	comment: 'Les wagons 4 et 5 à destination de Shibuya sont très remplis ; veuillez monter à l\'arrière du train.'
};
dayDatas[14] = {
	date: 14,
	title: 'Borne anti-moto',
	picture: 'images/14.jpg',
	comment: 'Quand il en a trois ou quatre, on dirait un troupeau.'
};
dayDatas[15] = {
	date: 15,
	title: 'Chanson sur place assise',
	picture: 'images/15.jpg',
	comment: 'Une comptine pour enfant qui parle d\'un raton-laveur, mais on peut s\'asseoir dessus.'
};
dayDatas[16] = {
	date: 16,
	title: 'Arrêt avant de traverser',
	picture: 'images/16.jpg',
	comment: 'Pas loin de la sortie d\'un centre pour enfants. Son copain le crocodile était à droite.'
};
dayDatas[17] = {
	date: 17,
	title: 'Monument au parc Inokashira',
	picture: 'images/17.jpg',
	comment: 'A la mémoire d\'un compositeur nommé Yoshinao Nakada ; une de ses chansons y est inscrite.'
};
/*
dayDatas[17] = {
	date: 17,
	title: 'Coucher de soleil sur la riviere Zenpukujigawa',
	picture: 'images/17.png',
	comment: 'This is a comment, longer than the title but still short'
};
*/
dayDatas[18] = {
	date: 18,
	title: 'Une grosse bouteille',
	picture: 'images/18.jpg',
	comment: 'Vendredi dernier, c\'était repas de fin d\'année. Moi j\'ai bu du jus d\'orange.'
};
dayDatas[19] = {
	date: 19,
	title: 'Panneau d\'affichage à Shibuya',
	picture: 'images/19.jpg',
	comment: 'Vendredi dernier, c\'était repas de fin d\'année. Moi je suis rentré un peu plus tard que prévu.'
};
dayDatas[20] = {
	date: 20,
	title: 'Un minibus sympa',
	picture: 'images/20.jpg',
	comment: 'J\'en ai vu un autre, en rouge et jaune, mais il faisait trop noir pour une photo.'
};
dayDatas[21] = {
	date: 21,
	title: 'Internet au LAWSON',
	picture: 'images/21.jpg',
	comment: 'On y fait des jeux de trains et des calendriers de l\'Avent. La lumière baisse un peu après 23h.'
};
dayDatas[22] = {
	date: 22,
	title: 'Pour le petit-déjeuner',
	picture: 'images/22.jpg',
	comment: 'Ce sera céréales. Mais à quoi ? Chocolat, fraise, framboise, ou patate douce ?'
};
dayDatas[23] = {
	date: 23,
	title: 'De l\'avion en Russie',
	picture: 'images/23.jpg',
	comment: 'Ils ont été obligés de dégivrer les ailes avant de nous laisser décoller.'
};
dayDatas[24] = {
	date: 24,
	title: 'Préparer le Réveillon',
	picture: 'images/24.jpg',
	comment: '8 œufs et 300 grammes de farine. On n\'a pas peser tout le gruyère rapé...'
};
dayDatas[25] = {
	date: 25,
	title: 'Joyeux Noël !',
	picture: 'images/25.jpg',
	comment: 'Encore un très bon Noël passé avec tout le monde. Vivement l\'année prochaine !'
};

function getDayData(date) {
	return dayDatas[date] || dayDatas[1];
}

var permutationArray = [0, 17, 13, 1, 20, 24, 3, 9, 19, 5, 25, 10, 11, 2, 15, 22, 21, 7, 6, 12, 16, 14, 8, 18, 4, 23];

for (var i = 1; i <= 25; ++i) {
	var date = permutationArray[i];
	
	var dayData = dayDatas[date] || {
		date: date,
		title: 'Hello',
		picture: 'images/1.jpg',
		comment: 'Long comment'
	};
	
	dayDatas[date] = dayData;
	
	calendar.appendChild(createDay(dayData));
	// Taking advantage of the fact that restoreCalendarDay sets the .opened class
	// to initialize calendarDay as already opened if needed
	restoreCalendarDay(date);
}




//overlayContent.appendChild(createDayModal(dayData));

