var postsHolder;
var trueMenu, fakeMenu, searchIndex;
var searchBarHTML,  searchBarElement;
var postsResults = [0, 0, 0, 0];
var lastPostId = 13;


function setArticle(titleHolder, contentHolder, tagsHolder, postFile) {

	var endInd = postFile.indexOf('\n') - 1;
	var currLine = postFile.substr(0, endInd);

	// TITLE
	var titleText = currLine;
	
	postFile = postFile.substr(endInd + 2);	
	endInd = postFile.indexOf('\n')-1;
	currLine = postFile.substr(0, endInd);
	
	// DATE
	// Ignore for now
	// console.log(currLine);

	postFile = postFile.substr(endInd + 2);	
	endInd = postFile.indexOf('\n')-1;
	currLine = postFile.substr(0, endInd);
	
	//IMAGE
	var imageLink = currLine;
	//imageLink = 'images/square-image.png';
	
	titleHolder.innerHTML = '<img src="' + imageLink + '" class="ui avatar image">' + titleText;
	
	postFile = postFile.substr(endInd + 2);	
	endInd = postFile.indexOf('\n')-1;
	currLine = postFile.substr(0, endInd);
	
	// TAGS
	var tags = currLine.split('#');
	var tagsHTML = '';
	for(var i in tags) {
		tagsHTML += (tagsHTML.length>0?' ':'') + '#' + tags[i];
	}
	tagsHolder.innerHTML = tagsHTML;
	
	postFile = postFile.substr(endInd + 2);	
	// Everything left is considered as the content of the post
	currLine = '<div class="ui header dividing" style="font-size: 0px;">.</div>' +  postFile;
	while (currLine.indexOf('\n') != -1)
		currLine = currLine.replace('\n', '<br>');
	
	currLine += '<br>'; // to make up for the tags being 'attached'
	
	// CONTENT
	// console.log(currLine);
	contentHolder.innerHTML = currLine;
}

function createArticle(postFile) {

	var htmlSkeleton = '<div class="ui header segment" style="display: inline-block;"><div class="ui header">'
		+ '<p id="titleHolder" class="article-title"><img src="_#_TITLE_IMAGE_#_" class="ui avatar image">_#_TITLE_#_</p></div>'
		+ '<div id="contentHolder" class="ui sub header content" style="text-align: left;">_#_CONTENT_#_</div>'
		+ '<div id="tagsHolder" class="ui bottom label attached content myTags">_#_TAGS_#_</div></div>';

	var endInd = postFile.indexOf('\n') - 1;
	var currLine = postFile.substr(0, endInd);

	// TITLE
	var titleText = currLine;
	htmlSkeleton = htmlSkeleton.replace('_#_TITLE_#_', titleText);
	
	postFile = postFile.substr(endInd + 2);	
	endInd = postFile.indexOf('\n')-1;
	currLine = postFile.substr(0, endInd);
	
	// DATE
	// Ignore for now
	// console.log(currLine);

	postFile = postFile.substr(endInd + 2);	
	endInd = postFile.indexOf('\n')-1;
	currLine = postFile.substr(0, endInd);
	
	//IMAGE
	var imageLink = currLine;
	htmlSkeleton = htmlSkeleton.replace('_#_TITLE_IMAGE_#_', imageLink);
	
	postFile = postFile.substr(endInd + 2);	
	endInd = postFile.indexOf('\n')-1;
	currLine = postFile.substr(0, endInd);
	
	// TAGS
	var tags = currLine.split('#');
	var tagsHTML = '';
	for(var i in tags) {
		tagsHTML += (tagsHTML.length>0?' ':'') + '#' + tags[i];
	}
	htmlSkeleton = htmlSkeleton.replace('_#_TAGS_#_', tagsHTML);
	
	postFile = postFile.substr(endInd + 2);	
	// Everything left is considered as the content of the post
	currLine = '<div class="ui header dividing" style="font-size: 0px;">.</div>' +  postFile;
	while (currLine.indexOf('\n') != -1)
		currLine = currLine.replace('\n', '<br>');
	
	currLine += '<br>'; // to make up for the tags being 'attached'
	
	// CONTENT
	// console.log(currLine);
	htmlSkeleton = htmlSkeleton.replace('_#_CONTENT_#_', currLine);

	//console.log(htmlSkeleton);
	
	return htmlSkeleton;
}

function formatArticleId(id) {
	fId = '' + id;
	
	while (fId.length < 3) {
		fId = '0' + fId;
	}
	fId = 'posts/' + languageSuffix + '/post' + fId  + '.txt';
	
	return fId;
}

function getAndAddArticle(article, callback) {
	var url = formatArticleId(article);
	var xmlDoc = new XMLHttpRequest();
		xmlDoc.open('GET', url, true);
		xmlDoc.onreadystatechange = function () {
			if (xmlDoc.readyState != XMLHttpRequest.DONE) return;
			postsHolder.innerHTML += createArticle(xmlDoc.responseText);
			$('.ui.sticky')
			  .sticky({
				context: '#content-posts',
				onStick: function(module) {
					fakeMenu.innerHTML = trueMenu.innerHTML;
				},
				onUnstick: function(module) {
					fakeMenu.innerHTML = '';
				}
			  })
			;
			//console.log('GOT ARTICLE ' + article);
			setTimeout(callback, 1000);
		}
	//console.log('Getting ' + url);
	xmlDoc.send();
}

function getAndAddArticles(articles, index) {
	if (typeof(index) === 'undefined') {
		index = articles.length - 1;
	}
	
	if (index >= 0) {
		getAndAddArticle(articles[index], function(){getAndAddArticles(articles, index - 1);});
	}
	else {
		//console.log('Done retrieving articles');
	}
}

function getLoadArticleBlock(postId) {
	var template = '<div id="previousPostLoader" class="ui header segment" style="display: inline-block;">'
    + '<div class="ui header">'
	+ '<p id="titleHolder" class="article-title"><a onclick="loadSingleArticle(__POST_ID__, 1);">__LOAD_PREVIOUS_POST__</a></p>'
    + '</div></div>';
	
	var loadPreviousPostText = (languageSuffix == 'fr'?'Charger l\'article précédent':'Load the previous post');
	
	template = template.replace('__POST_ID__', postId);
	template = template.replace('__LOAD_PREVIOUS_POST__', loadPreviousPostText);
	
	return template;
}

function loadSingleArticle(postId, element) {
	getAndAddArticle(postId, function(){
		if (typeof(element) !== 'undefined') {
				element = document.getElementById('previousPostLoader');
				element.parentNode.removeChild(element);
		}
		if (postId > 0) {
			postsHolder.innerHTML += getLoadArticleBlock(postId - 1);
		}	
	});
}

function resetPosts() {
postsHolder.innerHTML = '';
}

function initSearch() {
var url = 'tools/searchIndex_' + languageSuffix + '.json';
var xmlDoc = new XMLHttpRequest();

		xmlDoc.open('GET', url, true);
		xmlDoc.onreadystatechange = function () {
			if (xmlDoc.readyState != XMLHttpRequest.DONE) return;
			console.log('Index received!');
			//var start = Date.now();
			searchIndex = loadIndex(xmlDoc.responseText, undefined, true);
			/*var middle = Date.now() - start;
			console.log('Index loaded in ' + middle + 'ms');
			middle = Date.now();
			printResults(searchIndex.searchAST(new AST(AST.OR, new AST(AST.STRING, 'kagamine'), new AST(AST.STRING, 'hatsune'))), searchIndex);
			console.log('Index searched in ' + (Date.now() - middle) + 'ms');*/
		}
	//console.log('Getting ' + url);
	xmlDoc.send();
}

function getResultIcon(link) {
var template = "<img class=\"ui image circular searchImage\" src=\"" + link + "\">";
return template;
}

function searchPosts(terms) {
console.log('RESEARCH');
var termArray = terms.split(' ');

if (termArray.length < 1)
return;

var ast = new AST(AST.STRING, termArray[0]);

for (var i = 1; i < termArray.length; ++i) {
ast = new AST(AST.OR, new AST(AST.STRING, termArray[i]), ast);
}

var results = searchIndex.searchAST(ast);
console.log(results);
var ite = 1;

for (var result in results) {
if (ite > 4) break;

console.log(searchIndex.idMap[result]);

var iconLink = document.getElementById('result' + ite);

iconLink.onclick = function(postId){ return function(){console.log(postId);resetPosts(); loadSingleArticle(postId)};}(searchIndex.idMap[result].postId);
iconLink.innerHTML = getResultIcon(searchIndex.idMap[result].image);

++ite;
}

while(ite <= 4) {
var iconLink = document.getElementById('result' + ite);

iconLink.onclick = function(){};
iconLink.innerHTML = getResultIcon('images/square-image.png');
++ite;
}


}

function toggleSearchBar() {
if (searchBarElement != null) {
$('#menuBar').append(searchBarElement);
searchBarElement = null;
}
else {
searchBarElement = $('#searchBar').detach();
}
$(window).trigger('resize');
}

function loadRandomPost() {
var postId = Math.min(Math.floor(Math.random() * (lastPostId + 1)), lastPostId);

resetPosts();
loadSingleArticle(postId);
}

(function (window, document) {

    var layout   = document.getElementById('layout'),
        menu     = document.getElementById('menu'),
        menuButton = document.getElementById('menuButton'),
		titleHolder = document.getElementById('titleHolder'),
		contentHolder = document.getElementById('contentHolder'),
		tagsHolder = document.getElementById('tagsHolder');
	postsHolder = document.getElementById('content-posts');
	trueMenu = document.getElementById('trueMenu');
	fakeMenu = document.getElementById('fakeMenu');
	
    function toggleClass(element, className) {
        var classes = element.className.split(/\s+/),
            length = classes.length,
            i = 0;

        for(; i < length; i++) {
          if (classes[i] === className) {
            classes.splice(i, 1);
            break;
          }
        }
        // The className is not found
        if (length === classes.length) {
            classes.push(className);
        }

        element.className = classes.join(' ');
    }

    /*menuButton.onclick = function (e) {
        var active = 'active';

        e.preventDefault();
        toggleClass(layout, active);
        toggleClass(menu, active);
        toggleClass(menuButton, active);
    };//*/

	
	// var xmlDoc = new XMLHttpRequest();
	
	// xmlDoc.open('GET', 'posts/post000.txt', true);
	// xmlDoc.onreadystatechange = function () {
		// if (xmlDoc.readyState != XMLHttpRequest.DONE) return;
		// //console.log(xmlDoc.responseText);
		// setArticle(titleHolder, contentHolder, tagsHolder, xmlDoc.responseText);
	// }
	// xmlDoc.send();
	
	//getAndAddArticles(['post000', 'post001', 'post002', 'post003']);
	loadSingleArticle(lastPostId);
	initSearch();
		
}(this, this.document));

searchBarHTML = '\
<div id="searchBar" class="ui menu" style="width: 100%; z-index: 999; box-shadow: none !important; background-color: rgba(0,0,0,0) !important; margin-top:0px;">\
	<a class="item back-white" style="padding-top: 0.95em !important; padding-bottom: 0.95em !important;">\
		<div class="ui icon input" style="display: inline-block; position: relative;">\
			<form onsubmit="searchPosts(search.value); return false;"><input type="text" placeholder="' + (languageSuffix==='en'?'Search':'Recherche') + '..." name="search" class="prompt">\
			<i class="fa fa-search" style="position: absolute; right: 0.7em; display: inline-block; font-size: 1em; border: 0px black solid; top: 28%; opacity: 0.7;"></i>\
			</form>\
		</div>\
	</a>\
	<a id="result1" class="item back-white" style="padding: 0.45em 0.45em;">\
		<img class="ui image circular searchImage" src="images/square-image.png">\
	</a>\
	<a id="result2" class="item back-white" style="padding: 0.45em 0.45em;">\
		<img class="ui image circular searchImage" src="images/square-image.png">\
	</a>\
	<a id="result3" class="item back-white" style="padding: 0.45em 0.45em;">\
		<img class="ui image circular searchImage" src="images/square-image.png">\
	</a>\
	<a id="result4" class="item back-white" style="padding: 0.45em 0.45em;">\
		<img class="ui image circular searchImage" src="images/square-image.png">\
	</a>\
</div>\
';

searchBarElement = $.parseHTML(searchBarHTML);
