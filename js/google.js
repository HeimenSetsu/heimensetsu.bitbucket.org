// SearchEngine.js -- a Text Mining exercise
// duchem_g

var fs;
var util;
var path;

if (typeof document === 'undefined') {
fs = require('fs');
util = require('util');
path = require('path');
}

// Globals are bad, mmkay
var docCount = 0;

// Document
function Document(url, id) {
this.url = url;
this.text = fs.readFileSync(url, 'UTF-8');
this.id = id;
this.meta = getMetadata(url);
this.meta.url = url;
this.meta.id = this.id;

var postNum = /.*post([0-9]+)\.txt/g;
var match;
if (match = postNum.exec(url)) {
this.meta.postId = parseInt(match[1]);
console.log(url + ' -> ' + match[1]);
}
}

Document.prototype.analyze = function() {
this.processedText = processTextExtended(this.text, this.meta);
this.tokens = this.text.split(/[\s\.,-\/#!$%"\^&\*;:|><'{}=\\\-_`~()@]/).filter(function(str){return str.length > 1;});
}

function getMetadata(url) {
// I'm So Meta, Even This Acronym
var meta = [];
var data = fs.statSync(url);

// Metadata extraction
meta.size = data.size;
meta.mtime = data.mtime.toISOString().replace(/T/, ' ').replace(/\..+/, '');

return meta;
};

// Connector-related code
function fetchDocuments(url, recurse, urlMap) {
var documents = [];
recurse = recurse || true;
var files = fs.readdirSync(url);

for (var f in files) {
var file = path.resolve(url + '/' + files[f]);
if (fs.lstatSync(file).isDirectory() && recurse) {
documents = documents.concat(fetchDocuments(file, recurse, urlMap));
}
if (fs.lstatSync(file).isFile()) {
var id;

if (urlMap && urlMap[file] !== undefined) {
id = urlMap[file];
}
else {
id = docCount++;
}

documents.push(new Document(file, id));
}

}

return documents;
}

// Analyzer-related code ()
function processText(text) {
// lowercase-isation
text = text.toLowerCase();

// Remove accents
text = stripVowelAccent(text);

return text;
}

function processTextExtended(text, meta) {
var saveText = text;

//// FIRST WE RETRIEVE THE CASE-SENSITIVE IMAGE PATH
// Analyze the text itself
var endInd = text.indexOf('\n') - 1;
var currLine = text.substr(0, endInd);

// TITLE
text = text.substr(endInd + 2);	
endInd = text.indexOf('\n')-1;
currLine = text.substr(0, endInd);

// DATE
text = text.substr(endInd + 2);	
endInd = text.indexOf('\n')-1;
currLine = text.substr(0, endInd);

// IMAGE
var imageLink = currLine;
//// IMAGE PATH RETRIEVED

text = saveText;

// lowercase-isation
text = text.toLowerCase();

// Remove accents
text = stripVowelAccent(text);

var finalText = '';

// Analyze the text itself
var endInd = text.indexOf('\n') - 1;
var currLine = text.substr(0, endInd);

// TITLE
var titleText = currLine;
finalText += titleText;
	
text = text.substr(endInd + 2);	
endInd = text.indexOf('\n')-1;
currLine = text.substr(0, endInd);

// DATE
var dateText = currLine;
// console.log(currLine);

text = text.substr(endInd + 2);	
endInd = text.indexOf('\n')-1;
currLine = text.substr(0, endInd);

// IMAGE
// We already retrieved that path earlier
//var imageLink = currLine;

text = text.substr(endInd + 2);	
endInd = text.indexOf('\n')-1;
currLine = text.substr(0, endInd);

// TAGS
var tags = currLine.split('#');
var tagsHTML = '';
for(var i in tags) {
	tagsHTML += (tagsHTML.length>0?' ':'') + '#' + tags[i];
	finalText += ' ' + tags[i];
}

text = text.substr(endInd + 2);	
// Everything left is considered as the content of the post
currLine = text;
while (currLine.indexOf('\n') != -1)
	currLine = currLine.replace('\n', ' ');

// Retrieve information embedded in HTML anchors
var htmlNewLine = /<br>/g
while (currLine.match(htmlNewLine))
currLine = currLine.replace(htmlNewLine, " ");

// The best way to fight embedded tags is
// DOING THE SAME THING AGAIN AND AGAIN
for (var againAndAgain = 0; againAndAgain < 3; ++againAndAgain) { 
	var japaneseBoldText = /<b title="([^"]*)">([^<]*)<\/b>/g
	while (currLine.match(japaneseBoldText))
	currLine = currLine.replace(japaneseBoldText, "$1 $2 ");
	var japaneseItalicText = /<i title="([^"]*)">([^<]*)<\/i>/g
	while (currLine.match(japaneseItalicText))
	currLine = currLine.replace(japaneseItalicText, "$1 $2 ");

	var boldText = /<b[^>]*>([^<]*)<\/b>/g
	while (currLine.match(boldText))
	currLine = currLine.replace(boldText, "$1 ");
	var italicText = /<i[^>]*>([^<]*)<\/i>/g
	while (currLine.match(italicText))
	currLine = currLine.replace(italicText, "$1 ");
	var anchorText = /<a[^>]*>([^<]*)<\/a>/g
	while (currLine.match(anchorText))
	currLine = currLine.replace(anchorText, "$1 ");

	var listItemsText = /<li[^>]*>([^<]*)<\/li>/g
	while (currLine.match(listItemsText))
	currLine = currLine.replace(listItemsText, "$1 ");
	var listText = /<ul[^>]*>([^<]*)<\/ul>/g
	while (currLine.match(listText))
	currLine = currLine.replace(listText, "$1 ");

	var imgTag = /<img[^>]*>/g
	while (currLine.match(imgTag))
	currLine = currLine.replace(imgTag, "");

	var imgSubtext = /<p[^>]*>([^<]*)<\/p>/g
	while (currLine.match(imgSubtext))
	currLine = currLine.replace(imgSubtext, "$1 ");

	var imgSubtext = /<div[^>]*>([^<]*)<\/div>/g
	while (currLine.match(imgSubtext))
	currLine = currLine.replace(imgSubtext, "$1 ");

}

//console.log(currLine + '\n\n');

// Store relevant data
meta.title = titleText;
meta.date = dateText;
meta.image = imageLink;

return finalText;
}

function stripVowelAccent(str) {
// Found somewhere on StackOverflow, if I remember correctly
 var rExps=[
 {re:/[\xC0-\xC6]/g, ch:'A'},
 {re:/[\xE0-\xE6]/g, ch:'a'},
 {re:/[\xC8-\xCB]/g, ch:'E'},
 {re:/[\xE8-\xEB]/g, ch:'e'},
 {re:/[\xCC-\xCF]/g, ch:'I'},
 {re:/[\xEC-\xEF]/g, ch:'i'},
 {re:/[\xD2-\xD6]/g, ch:'O'},
 {re:/[\xF2-\xF6]/g, ch:'o'},
 {re:/[\xD9-\xDC]/g, ch:'U'},
 {re:/[\xF9-\xFC]/g, ch:'u'},
 {re:/[\xD1]/g, ch:'N'},
 {re:/[\xF1]/g, ch:'n'} ];

 for(var i=0, len=rExps.length; i<len; i++)
  str=str.replace(rExps[i].re, rExps[i].ch);

 return str;
}

// Index-related code
function Index(tree, idMap, urlMap) {
this.tree = tree || [];
this.idMap = idMap || [];
this.urlMap = urlMap || [];
}

Index.prototype.addDocToIndex = function(doc) {
	if (this.idMap[doc.id]) {console.log('ERROR');system.exit();}
	// The underscore is added to keep node from making the index
	// an integer, resulting in (very large) sparses arrays
	this.idMap['_' + doc.id] = doc.meta;
	this.urlMap[doc.meta.url] = doc.id;
	var pos = 0;
	for(var i in doc.tokens) {
	var data = doc.tokens[i].split('\t');
	var dict = this.tree;

	for(var j = 0; j < data[0].length; ++j){
		var c = data[0].charAt(j).toLowerCase();
		if (dict[c] === undefined) {
			dict[c] = {};
		}
			dict = dict[c];
		}

		// 'l' stands for list
		// I saved more than 10 kB by shaving off
		// those 3 characters.
		dict.l = dict.l || [];
		dict.l['_' + doc.id] = getPosAndOcc(dict.l['_' + doc.id], pos++);
	}
}

Index.prototype.addAllDocsToIndex = function(docs) {
for (var d in docs) {
this.addDocToIndex(docs[d]);
}
}

Index.prototype.search = function(word) {
var dict = this.tree;
var data = processText(word);

	for(var j = 0; j < data.length; ++j){
		var c = data.charAt(j);
		dict = dict[c];
		if (dict === undefined) {return [];}
	}

	return dict.l;
}

Index.prototype.save = function(filepath) {
// JSON.stringify is overrated. Seriously.
var data = '{ "tree": ';

data += getDictAsString(this.tree);

data += ', "idMap": ';

data += getDictAsString(this.idMap);

data += '}';

// Do not save the urlMap, as the information is redundant with
// idMap (it only allows for faster access)
// Rather, reconstruct it when loading the index

fs.writeFileSync(filepath, data);
}

Index.prototype.searchAST = function(ast) {

switch (ast.type) {
case AST.STRING:
return this.search(ast.left);
case AST.AND:
return ANDLists(this.searchAST(ast.left), this.searchAST(ast.right));
case AST.OR:
return ORLists(this.searchAST(ast.left), this.searchAST(ast.right));
case AST.NOT:
this.allIndexes = this.allIndexes || this.getAllIndexes();
return NOTLists(this.allIndexes, this.searchAST(ast.left));
break;
}
}

Index.prototype.getAllIndexes = function() {
var list = [];

for (var i in this.idMap) {
list[i] = Number.MAX_VALUE + '_' + 1;
}

return list;
}

Index.prototype.removeDocById = function(id) {
var url = this.idMap['_' + id].url;
var doc = new Document(url, -1);
doc.analyze();

for(var i in doc.tokens) {
	var data = doc.tokens[i].split('\t');
	var dict = this.tree;

	for(var j = 0; j < data[0].length; ++j){
		var c = data[0].charAt(j).toLowerCase();
		
		dict = dict[c];
			
	}

	if (dict.l) {
		delete dict.l['_' + id];
	}
	
}

delete this.idMap[id];
delete this.urlMap[doc.meta.url];

}

Index.prototype.removeDocByURL = function(url) {
var id = this.urlMap[path.resolve('.', url)];
this.removeDocById(id);
};

Index.prototype.explorePath = function(path, recurse) {
var docs = fetchDocuments(path, recurse, this.urlMap);

for (var doc in docs) docs[doc].analyze();

this.addAllDocsToIndex(docs);
}

function loadIndex(filePath, urlMap, treatAsFile) {
var index = new Index([], [], urlMap);
var data;

if (treatAsFile) {
data = JSON.parse(filePath);
} else {
data = JSON.parse(fs.readFileSync(filePath));
}

index.tree = data.tree;
index.idMap = data.idMap;

for (var i in index.idMap) {
var meta = index.idMap[i];
index.urlMap[meta.url] = i.toString().replace('_', '');
}

return index;
}

function getDictAsString(dict) {
if (typeof dict == 'string' || dict instanceof String) {
return '"' + dict.replace(/([\\])/g,'\\$1') + '"';
}

var ans = '{';

var mult = false;
for (var i in dict) {
if (mult) ans += ', ';

ans += '"' + i + '" :' + getDictAsString(dict[i]);

mult = true;
}

ans +='}';

if (!mult) {
return dict;
}

return ans;
}

function getPosAndOcc(old, newPos, countToAdd) {
if (countToAdd === undefined || countToAdd === null) {
countToAdd = 1;
}

old += '';
var num = old.split('_');

num[0] = Math.min(num[0], newPos);

if (num[0] + '' == 'NaN') {
num[0] = newPos;
}

if (parseInt(num[1])) {
num[1] = parseInt(num[1])+countToAdd;
}
else{
num[1] = countToAdd;
}

return num[0] + '_' + num[1];
}

function sortResults(results) {
var sorted = [];
var index = [];

for (var i in results) {
index.push(i);
}

index.sort(function(a, b){
	var numA = results[a].split('_');
	var numB = results[b].split('_');
	
	// numX[0] contains the first occurence
	// numX[1] contains the number of occurences
	if (numA[1] == numB[1]) {
	return numB[0] - numA[0];
	}

	return numA[1] - numB[1];
});


for (var i in index) {
sorted.unshift(index[i] + '_' + results[index[i]]);
}

return sorted;
}

// (Un)Compression functions, to reduce the serialized ASTs' size
// TODO: Erhm, not really that useful. Forget about it.
function compressText(text, chars) {

for(var i = 0; i < chars.length; ++i) {

var highestStreak = 4;
while (text.match(new RegExp(Array(highestStreak + 1).join(chars[i]) ) ) )
++highestStreak;

while (highestStreak > 3) {
	var currRegExp = new RegExp(Array(highestStreak + 1).join(chars[i]) );
	while (text.match(currRegExp))
		text = text.replace(currRegExp, chars[i] + highestStreak + chars[i]);
		
	console.log('Compressed ' + chars[i] + '*' + highestStreak);
	--highestStreak;
}
}

return text;
}

function uncompressText(text, chars) {
for(var i = 0; i < chars.length; ++i) {

var regExp = new RegExp(chars[i] + "([0-9]+)" + chars[i]);
var match = null;
while(match = regExp.exec(text)) {
	console.log(match[1] + '_YOLO');
	text = text.replace(regExp, Array(parseInt(match[1]) + 1).join(chars[i]) );
}
}

return text;
}

// AST
function AST(type, left, right) {
this.type = type;
this.left = left;
this.right = right;
}

function ORLists(list1, list2) {
for (var i in list1) {
list2[i] = getPosAndOcc(list2[i],parseInt(list1[i].split('_')[0]), parseInt(list1[i].split('_')[1])) || list1[i];
}

return list2;
}

function ANDLists(list1, list2) {
var list = [];
for (var i in list1) {
if (list2[i] !== undefined) {
list[i] = getPosAndOcc(list2[i],parseInt(list1[i].toString().split('_')[0]), parseInt(list1[i].toString().split('_')[1]));
}
}

for (var i in list2) {
if (list1[i] !== undefined) {
list[i] = getPosAndOcc(list1[i],parseInt(list2[i].toString().split('_')[0]), parseInt(list2[i].toString().split('_')[1]));
}
}


return list;
}

function NOTLists(list1, list2) {
var list = list1;

for (var i in list2) {
if (list[i]) delete list[i];
}

return list;
}

AST.STRING = 0;
AST.AND = 1;
AST.OR = 2;
AST.NOT = 3;

// Multi-generational index

function GenerationalIndex() {
this.generations = [];
this.urlMap = [];
this.docTruth = [];
}

GenerationalIndex.prototype.addGeneration = function(filePath) {
var newIndex = loadIndex(filePath, this.urlMap);

this.generations.push(newIndex);

for (var i in newIndex.idMap) {
this.docTruth[i] = this.generations.length - 1;
}

}

GenerationalIndex.prototype.search = function(str) {
var ans = [];

for (var i = 0; i < this.generations.length; ++i) {
var list = this.generations[i].search(str);

for (var docId in list) {
if (this.docTruth[docId] == i) {
ans[docId] = list[docId];
}
}

}

return ans;
}

GenerationalIndex.prototype.searchAST = function(ast) {
var ans = [];

for (var i = 0; i < this.generations.length; ++i) {
var list = this.generations[i].searchAST(ast);

for (var docId in list) {
if (this.docTruth[docId] == i) {
ans[docId] = list[docId];
}
}

}

return ans;
}

// Helper functions for the demo

function deleteFolderRecursive(path) {
// Found somewhere on stackOverflow. Again.
    var files = [];
    if( fs.existsSync(path) ) {
        files = fs.readdirSync(path);
        files.forEach(function(file,index){
            var curPath = path + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};

function printResults(results, sourceIndex) {
if ((typeof index !== 'undefined') && !sourceIndex) {
	sourceIndex = index;
}
var hasResults = false;
for (var i in results) {
var	str = '';
str = '----Document #' + i.toString().replace('_', '') + '----\n';
str += 'First word occurrence: ' + results[i].split('_')[0] + '\n';
str += 'Word match count: ' + results[i].split('_')[1] + '\n';
str += 'URL: ' + sourceIndex.idMap[i].url + '\n';
str += 'Size: ' + sourceIndex.idMap[i].size + ' bytes\n';
str += 'Last modified on: ' + sourceIndex.idMap[i].mtime + '\n';

		
console.log(str);
hasResults = true;
}

if (!hasResults) {
console.log('No documents matched the search');
}

console.log('\n\n');

}

function printSortedResults(results) {
var hasResults = false;
for (var i in results) {
var id = '_' + results[i].split('_')[1];

var str = '';
	
str = '----Document #' + id.replace('_', '') + '----\n';
str += 'First word occurrence: ' + (parseInt(results[i].split('_')[2]) + 1) + '\n';
str += 'Word match count: ' + results[i].split('_')[3] + '\n';
str += 'URL: ' + index.idMap[id].url + '\n';
str += 'Size: ' + index.idMap[id].size + ' bytes\n';
str += 'Last modified on: ' + index.idMap[id].mtime + '\n';


console.log(str);
hasResults = true;
}

if (!hasResults) {
console.log('No documents matched the search');
}

console.log('\n\n');
}

function headerPrint(title) {
var str = '********\n' + title + '\n********\n';
console.log(str);
}




if (typeof exports !== 'undefined') {
exports.Index = Index;
exports.AST = AST;
exports.loadIndex = loadIndex;
}




// ********************************
// *        STUF HAPPENS          *
// ********************************











/*
//// Usage

var currPath = './vocaloidData/';
var index = new Index();
//  1. Index creation
headerPrint('1. Index creation');

deleteFolderRecursive(currPath);
fs.mkdirSync(currPath);
fs.writeFileSync(currPath + '01', 'weekender girl san kyuu alice human sacrifice antibeat koi wa sensou melt matryoshka magnet distorted princess persona alice whiteout rinne');
fs.writeFileSync(currPath + '02r', 'worst carnival melancholic sadistic love meltdown sweet magic alice human sacrifice i aru fanclub chemical emotion');
fs.writeFileSync(currPath + '02l', 'butterfly right shoulder alice human sacrifice Aa, Subarashiki NYANsei ');
fs.writeFileSync(currPath + '03', 'double lariat magnet just be friends answer toeto heaven\'s door no logic rainbow afternoon happy synthesizer');
index.explorePath(currPath);

//  2. Simple search
headerPrint('2. Simple search');

printResults(index.search('antibeat'));
printResults(index.search('alice'));
printResults(index.search('melancholic'));
printResults(index.search('magnet'));
printResults(index.search('NOTHING'));

//  3. AST search
//     Here, we want to retrive the docuemnts describing all Vocaloids that
//     sang in either 'Alice of Human Sacrifice' or 'Magnet' while excluding those
//     that sang as lead in 'Aa, Subarashiki Nyansei'
//
//     That is to say, documents 01, 02r and 03.
headerPrint('3. AST search');

var ast = new AST(AST.AND,
				  new AST(AST.OR,
						  new AST(AST.STRING, 'AlIcE'),
						  new AST(AST.STRING, 'MaGnEt')),
				  new AST(AST.NOT,
						  new AST(AST.STRING, 'subarashiki')));

printResults(index.searchAST(ast));


//  4. Removing a document
headerPrint('4. Removing a document');

printResults(index.search('butterfly'));
//     'Butterfly on your Right Shoulder' was originally sang by Rin, not Len.
//     We have to update documents 02r and 02l !
index.removeDocByURL(currPath + '02l');
fs.writeFileSync(currPath + '02r', 'worst carnival melancholic sadistic love meltdown sweet magic alice human sacrifice i aru fanclub chemical emotion butterfly right shoulder');
fs.writeFileSync(currPath + '02l', 'alice human sacrifice Aa, Subarashiki NYANsei');
index.explorePath(currPath);

printResults(index.search('butterfly'));

// Note that the document 02r is indexed as #4 now (instead if #1),
// since it was removed and readded

//  5. Saving and reloading indexes
headerPrint('5. Saving and reloading indexes');

index.save('index.json');
index = null;
index = loadIndex('index.json');
printResults(index.search('butterfly'));


//  6. Generational index
//     Each generation has to be loaded from a JSON index file
//     Let us start by creating some generations
headerPrint('6. Generational index');

docCount = 0;
var index = new Index();
fs.unlinkSync(currPath + '03');
index.explorePath(currPath);
index.save('FRLG.json');
docCount = 0;
index = new Index();
fs.writeFileSync(currPath + '03', 'double lariat magnet just be friends answer toeto heaven\'s door no logic rainbow afternoon happy synthesizer');
index.explorePath(currPath);
index.save('HGSS.json');
docCount = 0;
index = new Index();
fs.writeFileSync(currPath + '02l', 'such useless much worst wow');
index.explorePath(currPath);
index.save('ORAS.json');

var genInd = new GenerationalIndex();

// First gen
genInd.addGeneration('FRLG.json');
// Should only return the document 01 for now
printResults(genInd.search('magnet'));

// Second gen
genInd.addGeneration('HGSS.json');
// Should now return both document 01 and 03
printResults(genInd.search('magnet'));
// Should not return anything
printResults(genInd.search('useless'));

// Third gen
genInd.addGeneration('ORAS.json');
// Should now return the document 02
printResults(genInd.search('useless'));

//  7. Sorted search
headerPrint('7. Sorted search');

docCount = 0;
var index = new Index();
deleteFolderRecursive(currPath);
currPath = './sortData/';
deleteFolderRecursive(currPath);
fs.mkdirSync(currPath);
fs.writeFileSync(currPath + 1, 'one two three one');
fs.writeFileSync(currPath + 2, 'three two one one');
fs.writeFileSync(currPath + 3, 'two one three two');
fs.writeFileSync(currPath + 4, 'three one two two');
fs.writeFileSync(currPath + 5, 'three one two three');
fs.writeFileSync(currPath + 6, 'two one three three');
index.explorePath(currPath);

printSortedResults(sortResults(index.search('one')));

//  8. Removing a document in a generational index
//  NOPE NOPE NOPE

// Clean up

deleteFolderRecursive(currPath);
fs.unlinkSync('index.json');
fs.unlinkSync('FRLG.json');
fs.unlinkSync('HGSS.json');
fs.unlinkSync('ORAS.json');

*/
