First steps in Tokyo (I): Akihabara
21/02/2015
images/post003/mikuBillboard0.jpg
tokyo#akihabara#figures#doujins#toranoana#vocaloid#hatsune#miku#gochiusa
One day or another, I was bound to go to Tokyo; and I decided that today was actually perfect for that purpose. Leaving the residence around 8:24 A.M., I took the cycling road until the Tsukuba station, where I left my bike in a bike park (<i class="fa fa-yen"></i>100 a day).
Then, after an album of Louise Attaque, I arrived at the terminus, which was also my first target: <b title="Akihabara">秋葉原</b>.

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post003/mikuBillboard1.jpg">
<i>A giant Miku billboard: that is the kind of sight I like
</i></p>
Since I did not have a particular thing in mind, I started to wander along the main street, looking at the stores on both sides. When I passed a figures shop that was just opening (a 'Hobby Figures', maybe? I do not remember its name), I decided to go and take a look inside. Taking pictures was prohibited, so I will keep it short: there were two entire shelf dedicated to giant (I mean, the size of my forearm) One Piece figures which were quite detailled, and I got to see a lot of Vocaloid figures I only saw on the Internet before. Some of them were really nice, others were posed too provocatingly for my taste, but oh well. While I found at least one Rin figure that was decently posed, I was not there to buy, so I left the shop and went on with my trip.

In a smaller street just behind the main one, I found some people waiting in line in front of a cafe. A quick look and a flyer helped me understand that they were running a promotional menu based on the <b title="Gochuumon wa usagi desu ka ?">ご注文はうさぎでうか?</b>, 'Is the order a rabbit?' anime/manga. It was to end on the 22nd of February, and I did not have time to wait for it today; maybe another time, I guess.

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post003/gochiUsaFlyer.jpg">
<i>I still took the flyer, because GochiUsa.
</i></p>
I entered several game shops, mostly to head to the retro gaming section. Japanese boxes are smaller than American/European boxes and do not have the same height/width ratio: it makes them feel kinda cute. The best prices I found for the original Pokémon games were around <i class="fa fa-yen"></i>1600-2000 for a boxed game. I am still wondering if I should buy those or not; since I could not find Green boxed, it makes the whole package a little less worth it.

Then I also went to Toranoana. Like with the game stores from before, lots and lots of things were on the shelves and walls, but there was very little space between them to move. Since I was in with my winter coat and my backpack, I always felt like I was gonna bump into something while walking, but in the end I did not.
On the first floor, I could check the Blu-Ray and DVD releases. The price of those is crazy high: I could not find an anime I was into for less than <i class="fa fa-yen"></i>5000 the box with TWO episodes of 24 minutes with both the opening and ending songs. A entire cour is TWELVE to FOURTEEN episodes long, which values the whole at roughly <i class="fa fa-yen"></i>3 0000-3 5000. That is ~230-270<i class="fa fa-euro"></i> for something like 4 to 5 hours of content. And most of the ones I saw were around <i class="fa fa-yen"></i>6000, with one even reaching <i class="fa fa-yen"></i>7300. Crazy high. And to think I want both Kiniro Mosaic and Hyouka... my wallet is already crying.

The other floors contained doujin works, i.e. stuff made by independant artists, either alone or under the name of a circle. I got tempted twice (a reedition of Touhou 6 for <i class="fa fa-yen"></i>1000 and a Vocaloid album by the 'No One Hears' circle from the Winter Comiket), but ended up resisting (since I will come back anyway :) ). The top two or three floors were marked 'for the ladies'; being unable to read this (until I saw the English sign when going down the stairs), I walked in and started looking at the exposed works. It took me three shelves of half-naked men in lying poses to look up and see the signs which, among kanjis I could not read, read the letters 'BL'. As this is still not a thing for me, I quickly stepped back, down, and exited the building.

What happened to me next ? To be continued in <b>Akihabara (pt. 2)</b>!

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post003/chinoBillboard.jpg">
<i>Even Chino wants to know what happened next!
</i></p>