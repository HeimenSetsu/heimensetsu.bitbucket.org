Beyond the moutain
17/03/2015
images/post009/thickShutters.jpg
tsukubasan#bike#fields#trip#mountain#shrine#agglomerate#bridges#one-eyed
There are two kind of interns here: those who -- aw, damn it, I think I already used that intro. Everything still holds, except the others tried to go to some kind of meeting for international people. This post does not tell of their tale at all.
This week, we decided to pay a visit to the Mount Tsukuba, which can be seen from the AIST campus if the weather is clear enough. Since we are the best at timing those kind of trip, we chose to do it on a cloudy Sunday. Actually, Saturday would have been a better day, however we chose to trust some kind of magician who pretended to know the weather <i>from the future</i>. You should always be wary of people pretending to know the future; I think ?
Also, Tsukuba-san is roughly 18 kilometers from our residence, which is quite a long way to go (without even thinking about the return trip). Fortunately for us, there is a bus line that goes the mountain itself, sparing us the long road and part of the ascent to the top.

However... well, you know... buses are for losers, m'kay ?

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post009/tsukubasanFromFields.jpg">
<i>Alas, we still had a long way to go
</i></p>
The first part of the ride was actually quite pleasant. The directions used mostly little roads, we stopped in front of a shrine (because <i>why not</i>?), and went through a lot of fields. In fact, one segment of the road was a 4km straight line through neatly arranged rectangular fields, still muddy from the rain that poured down the night before. 'Straight line', did I say? My mistake here, I meant <i>almost</i> straight. <b>Of course</b>, someone just <b>had</b> to put his/her house <b><i>right in the middle</i></b> of it.

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post009/googleMapHouseInFields.jpg">
<i>I forgot to take a picture, but you can see it on Google Street
</i></p>
Continuing on our trip, we went through an agglomerate of small habitations. I cannot say "village", because we were still in Tsukuba; the cities here are much more spread out than in France, and we technically never left Tsukuba at all on that day. Thanks to some kind of dark and ancient art, my phone's GPS chip does not need to be connected to a network to be able to geolocate itself, which means we could use it to navigate along the precomputed itinerary (or get back to it after getting lost somewhere).
Back to the houses though; there was a small stream of water going through the streets, but inhabitants of those houses still needed to have a way to get into their habitations. I do not think I would trust them to help me with some kind of building-related issue.

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post009/housesAndBridges.jpg">
<i>That is a valid way of overcoming an obstacle though 
</i></p>
Do I have something more to tell about this part of the trip? Oh yeah, we crossed an intersection equipped with a one-eyed traffic light.

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post009/cyclopTraffic.jpg">
<i>I guess this is interesting. I mean, I never saw one before; how does it even work?
</i></p>
Anyway, once arrived near Tsukuba-san, the roads and streets started to get a little bit more steep than before. We had to get down from our bikes and walk them to a car park, where we left them for the rest of the day. After eating a little something we prepared beforehand (I got myself some rice with an egg sandwich and a chocolate cream sandwich. Sweet), we walked to the Tsukuba-san Shrine.

I guess I only told you about half of my day here. To be continued in <b>Beyond the mountain (pt. 2)</b> !
