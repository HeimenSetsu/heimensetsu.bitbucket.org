Par-delà la montagne (pt. 2)
31/03/2015
images/post010/handOnRock.jpg
tsukubasan#voyage#montagne#rochers#racines#montée#nuages#kagamine rin
Pour continuer de là où je vous ai laissé la fois passée, plein de photos dans tous les sens.

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post010/rootStaircase.jpg">
<i>Un escalier de racines au début de la piste
</i></p>
<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post010/signToTheTop.jpg">
<i>Encore deux bons kilomètres avant d'atteindre le sommet
</i></p>
<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post010/thermometer.jpg">
<i>Le thermomètre indiquait 6.5°c, mais l'effort nous a tenu chaud
</i></p>
<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post010/deepStaircase.jpg">
<i>Remarquez comme l'escalier s'enfonce au loin dans la forêt
</i></p>
Une fois arrivé au sommet, nous n'avons pas recontré grand monde. Un homme mangeait un plat chaud assis sur un banc, et deux employés remettaient leur magasin en ordre avant de fermer boutique (après tout, il était tard. Du genre, 16h17).
Mais qu'importe, il nous était enfin donné d'admirer la vue d'en haut.

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post010/clouds.jpg">
<i>Ah, quel charman--
</i></p>
<b>Attendez un peu, <i>je m'en souviens maintenant</i></b>: puisque le voyage s'est fait en un jour <i>nuageux</i>, les nuages nous ont presque complètement bloqué la vue. Quel dommage. On a pris des photos à la place.

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post010/mountainAltar.jpg">
<i>Un petit autel perdu dans la montagne
</i></p>
<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post010/rinAtTheTop.jpg">
<i>J'ai emmené Rin avec moi, car elle est au dessus de <i>tout</i>
</i></p>

Nous avons commencé la descente vers 17h20, et il a été assez facile de comprendre pourquoi tous les gens que nous avions croisés à l'allée descendait de la montagne. La nuit est tombé assez rapidement, et j'ai dû faire la dernière demi-heure de la piste au flash de mon téléphone. J'ai quand même divergé de la-dite piste vers la fin (je pouvais déjà voir les lumières du temple, donc tout allait bien). Ma descente aura ainsi duré 70 minutes, soit le temps moyen indiqué sur les cartes. Joli score, si je puis me permettre.
Enfin, après un long retour à vélo, j'ai pu rentrer à ma chambre à Sakura-kan avant 21 heures ; parce que 20h50 vient avant 21 heures.

J'ai eu beaucoup, <i>beaucoup</i> de courbatures les 4 jours suivants.
