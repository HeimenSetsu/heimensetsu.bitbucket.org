First steps in Tokyo (II): Asakusa (pt. 2)
02/03/2015
images/post006/houzoumonSideLantern.jpg
tokyo#asakusa#senso-ji#houzoumon#ni-ou#sandal#owaraji#incense#fortune#ni#kannon
Since I ignored most thing about the temple when preparing my trip, I was not sure about what I should be expecting; so I mostly wandered in a effort to see as many things as I could. As such, instead of a structured post, let me just dump some pictures with an occasional comment.

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post006/houzoumon.jpg">
<i>The entrance of the temple, </i><b title="Houzoumon">宝蔵門</b><i>
</i></p>
According to my Japanese dictionary, the first kanji (<b title="hou">宝</b>) means "treasure", the second one (<b title="zou">蔵</b>) "hide", and you already recognize the last one (<b title="mon">門</b>) as the "gate". This makes sense since this gate exists to store the valuable sutras of the Sensou-ji.
Also, between the Kaminarimon and the Houzoumon was the <b title="Nakamise">仲見世</b>, a street filled with souvenir shops. Not really that interesting, but the backstreets were interesting (and sadly not in any of the pictures I took).

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post006/ni-ouOpen.jpg">
<i>A </i><b title="Ni-ou">仁王</b><i>, guardian of the Houzoumon
</i></p>
This Ni-ou is an "open mouth style", and was on the left when you are entering the temple. On the right was another Ni-ou, this one "closed mouth style". Both were <i>so</i> frightening they had to be enclosed so that they could not destroy us. Sadly, this makes for so-so pictures.

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post006/oWaraji.jpg">
<i>A </i><b title="O-Waraji">大わらじ</b><i>, "big sandal"
</i></p>
There was one sandal on each side of the gate. Those traditional sandals were made by 800 citizens, and each one is 4.5 meters high and weights 2.500 kilograms. They represent the power of the Ni-ou and are thus a protection against evils.

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post006/sensou-ji.jpg">
<i>The </i><b title="Sensou-ji">浅草寺</b><i>
</i></p>
So much people. The buildings on the left and the right were to draw your fortune. In the middle, you can see an incense pit. You are supposed to draw it toward your head, it purifies against evil spirits I think.

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post006/fortuneNi.jpg">
<i>As you can see, I got to draw one in the </i><b title="ni">二</b><i>　drawer.<br>It was a "better fortune".
</i></p>
This picture is my favourite from the ones I took on that day (let us not say "the only one I like", that would be sad ...). I had to focus the lens manually, since the stick was quite close from my camera, but I definitely felt something when I took the shot.

<p style="font-size: 12px; text-align: center;">
<img class="ui centered image rounded bordered" style="width: 90%; height:20%" src="images/post006/incensePit.jpg">
<i>The more incense the better. There was so much it smelled like smoke.
</i></p>
The temple itself is also known as <b title="Asakusa Kannon">浅草観音</b>, Kannon being a Buddhist deity of Mercy. This is Japan's oldest buddhist temple, and it is still used for prayers (I actually learned the ritual, but once I arrived could not think of anything worth praying for).
The legends say that three fishermen got a rock stuck in their fishing net, which later told them in a dream to build the temple. The rock was in fact a statuette of Kannon.
On the inside of the temple, you could also buy charms to get good luck, or any kind of good effect.

Well, the other pictures I took were not so great (I blame my camera. Although my phone has a fisheye effect, it still takes better pictures with the same resolution), so I think I will stop there. After visiting the temple, I tried some other rice crackers (cheaper than the first one, but not as good. Still pretty good) and then went back to Tsukuba.

While it took my more than a week to write all of this out, I finally got around to finish it. The next posts will return to an  aimless blabber about stuff. See ya!
