var searchModule = require('../js/google.js');

function printResults(results) {
var hasResults = false;
for (var i in results) {
var	str = '';
str = '----Document #' + i.toString().replace('_', '') + '----\n';
str += 'First word occurrence: ' + results[i].split('_')[0] + '\n';
str += 'Word match count: ' + results[i].split('_')[1] + '\n';
str += 'URL: ' + index.idMap[i].url + '\n';
str += 'Post ID: ' + index.idMap[i].postId + '\n';
str += 'Size: ' + index.idMap[i].size + ' bytes\n';
str += 'Last modified on: ' + index.idMap[i].mtime + '\n';

		
console.log(str);
hasResults = true;
}

if (!hasResults) {
console.log('No documents matched the search');
}

console.log('\n\n');

}

function printSortedResults(results) {
var hasResults = false;
for (var i in results) {
var id = '_' + results[i].split('_')[1];

var str = '';
	
str = '----Document #' + id.replace('_', '') + '----\n';
str += 'First word occurrence: ' + (parseInt(results[i].split('_')[2]) + 1) + '\n';
str += 'Word match count: ' + results[i].split('_')[3] + '\n';
str += 'URL: ' + index.idMap[id].url + '\n';
str += 'Size: ' + index.idMap[id].size + ' bytes\n';
str += 'Last modified on: ' + index.idMap[id].mtime + '\n';


console.log(str);
hasResults = true;
}

if (!hasResults) {
console.log('No documents matched the search');
}

console.log('\n\n');
}


var languages = ['en', 'fr'];

for(var i in languages) {
	var language = languages[i];
	var index = new searchModule.Index();
	index.explorePath('../posts/' + language);

	printResults(index.search('rin'));

	index.save('searchIndex_' + language + '.json');
	console.log('\nSaved the ' + language + ' index!\n');
}